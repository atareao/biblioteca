<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!doctype html>
<html lang="es">
    <head>
        <title>La Biblioteca</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">

        <!-- Material Desing Icons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/materialdesignicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/mdi-for-bootstrap.css">

        <!-- Optional JavaScript -->
        <!-- jQuery first -->
        <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
        <!--
        <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/<?php echo $script; ?>"></script>
        -->

    </head>
    <body>
        <div id="main-toolbar">
            <ul>
                <li><span id="breadcrumb"><?php echo $title2; ?></span></li>
            </ul>
        </div>
        <div id="panel-lateral">
            <nav class="main-navbar" role="navigation">
                <div id="button-desplegable">
                    <span id="desplegable" class="mdi mdi-chevron-left"></span>
                </div>
                <div id="app-name">
                    <h1><?php echo $main_title; ?></h1>
                </div>
                <div id="app-icono">
                    <img src="<?php echo base_url(); ?>images/logo.png">
                </div>
                <div id="main-menu">
                  <ul>
                    <li class='menu-item'>
                        <a href="#">
                            <div class='menu-item-container mdi mdi-chevron-right'>
                                <span class='menu-item-label'>Administración</span>
                                <span class="mdi mdi-account"></span>
                            </div>
                        </a>
                        <ul>
                            <li class='menu-item'>
                                <a href="<?php echo base_url(); ?>paises/index">
                                    <div class="menu-item-container">
                                        <span class='menu-item-label'>Paises</span>
                                        <span class="mdi mdi-account"></span>
                                    </div>
                                </a>
                                <ul>
                                    <li class='menu-item'>
                                        <a href="<?php echo base_url(); ?>paises/add">
                                            <div class="menu-item-container">
                                                <span class='menu-item-label'>Nuevo</span>
                                                <span class="mdi mdi-account"></span>
                                            </div>
                                    </li>
                                </ul>
                            </li>
                            <li class='menu-item'>
                                <a href="<?php echo base_url(); ?>editoriales/index">
                                    <div class="menu-item-container">
                                        <span class='menu-item-label'>Editoriales</span>
                                        <span class="mdi mdi-account"></span>
                                    </div>
                                </a>
                                <ul>
                                    <li class='menu-item'>
                                        <a href="<?php echo base_url(); ?>editoriales/add">
                                            <div class="menu-item-container">
                                                <span class='menu-item-label'>Nueva</span>
                                                <span class="mdi mdi-account"></span>
                                            </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class='menu-item'>
                        <a href="#">
                            <div class="menu-item-container  mdi mdi-chevron-right">
                                <span class='menu-item-label'>Usuarios</span>
                                <span class="mdi mdi-account"></span>
                            </div>
                        </a>
                        <ul>
                            <li class='menu-item'>
                                <a href="#">
                                    <div class="menu-item-container">
                                        <span class='menu-item-label'>Modificar</span>
                                        <span class="mdi mdi-account"></span>
                                    </div>
                                </a>
                            </li>
                            <li class='menu-item'>
                                <a href="#">
                                    <div class="menu-item-container">
                                        <span class='menu-item-label'>Reportar</span>
                                        <span class="mdi mdi-account"></span>
                                    </div>
                                </a>
                            </li>
                            <li class='menu-item'>
                                <a href="#">
                                    <div class="menu-item-container">
                                        <span class='menu-item-label'>Separated link</span>
                                        <span class="mdi mdi-account"></span>
                                    </div>
                                </a>
                            </li>
                            <li class='menu-item'>
                                <a href="#">
                                    <div class='menu-item-container'>
                                        <span class='menu-item-label'>Informes</span>
                                        <span class="mdi mdi-account"></span>
                                    </div>
                                </a>
                            </li>
                      </ul>
                    </li>
                    <li class='menu-item'>
                        <a href="#">
                            <div class='menu-item-container'>
                                <span class='menu-item-label'>Libros</span>
                                <span class="mdi mdi-account"></span>
                            </div>
                        </a>
                    </li>
                    <li class='menu-item'>
                        <a href="#">
                            <div class='menu-item-container'>
                                <span class='menu-item-label'>Tags</span>
                                <span class="mdi mdi-account"></span>
                            </div>
                        </a>
                    </li>
                  </ul>
                </div>
            </nav>
        </div>
        <div id="main">
            <div id="main-data">
