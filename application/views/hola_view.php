<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
    <div class='col-md-12 right_col'>
        <div id='information'></div>

    <p>Esta es la nueva página estática que hemos creado, correspondiente a nuestro primer controlador y a nuestra primera vista</p>
    </div>
    <?php
    $data = array(
        4  => 'http://example.com/news/article/2018/02/04/',
        12 => 'http://example.com/news/article/2018/02/12/',
        20 => 'http://example.com/news/article/2018/02/20/',
        28 => 'http://example.com/news/article/2018/02/28/'
    );
    echo $this->calendar->generate(2018, 2, $data);
    ?>
    <br/>
    <?php
    $data = array(
        array('N° atómico', 'Nombre', 'Símbolo'),
        array('57', 'Lantano', 'La'),
        array('58', 'Cerio', 'Ce'),
        array('59', 'Praseodimio', 'Pr'),
        array('60', 'Neodimio', 'Nd'),
        array('61', 'Prometio', 'Pm'),
        array('62', 'Samario', 'Sm'),
        array('63', 'Europio', 'Eu'),
        array('64', 'Gadolinio', 'Gd'),
        array('65', 'Terbio', 'Tb'),
        array('66', 'Disprosio', 'Dy'),
        array('67', 'Holmio', 'Ho'),
        array('68', 'Erbio', 'Er'),
        array('69', 'Tulio', 'Tm'),
        array('70', 'Iterbio', 'Yb'),
        array('71', 'Lutecio', 'Lu')
    );
    echo $this->table->generate($data);
