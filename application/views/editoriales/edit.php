<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
                <div class='col-md-12 right_col'>
                    <div id='information'></div>
                        <?php echo validation_errors(); ?>
                        <?php echo form_open('editoriales/edit'); ?>

                        <input type="hidden" name="id" value="<?php echo $editorial['id']; ?>"/>

                        <label for="nombre">Nombre de la editorial:</label><br/>
                        <input type="text" name="nombre" value="<?php echo $editorial['nombre']; ?>"/>
                        <div class="error"><?php echo form_error('nombre'); ?></div>

                        <input type="submit" name="update" value="Actualizar" />

                        <?php echo form_close();?>
                    </div>
                </div>