<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editoriales_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get($nombre = FALSE)
    {
        if ($nombre === FALSE)
        {
            $this->db->order_by('id', 'asc');
            $query = $this->db->get('editoriales');
            return $query->result_array();
        }
        $this->db->order_by('id', 'asc');
        $query = $this->db->get_where('editoriales', array('nombre' => $nombre));
        return $query->result_array();
    }
    public function add()
    {
        $data = array(
            'nombre'   => $this->input->post('nombre'),
        );
        return $this->db->insert('editoriales', $data);
    }
    public function delete($id)
    {
        $this->db->delete('editoriales', array('id' => $id));
    }
    function get_editorial_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('editoriales');
        return $query->result_array();
    }
    function update($id, $nombre)
    {
        $this->db->where('id', $id);
        $this->db->set('nombre', $nombre);
        return $this->db->update('editoriales');
    }
}
