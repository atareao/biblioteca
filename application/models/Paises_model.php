<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paises_model extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }
    public function get($nombre = FALSE)
    {
        if ($nombre === FALSE)
        {
            $this->db->order_by('id', 'asc');
            $query = $this->db->get('paises');
            return $query->result_array();
        }
        $this->db->order_by('id', 'asc');
        $query = $this->db->get_where('paises', array('nombre' => $nombre));
        return $query->row_array();
    }
    public function insert($nombre)
    {
        $data = array(
            'nombre'   => $nombre,
        );
        return $this->db->insert('paises', $data);
    }
    public function edit($id, $nombre)
    {
        $this->db->set('nombre', $nombre);
        $this->db->where('id', $id);
        return $this->db->update('paises');
    }
    public function delete($id)
    {
        $this->db->delete('paises', array('id' => $id));
    }

}
