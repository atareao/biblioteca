<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paises extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('paises_model');
        $this->load->helper('url_helper');
    }
    public function index()
    {
        $data['paises'] = $this->paises_model->get();
        $data['main_title'] = 'Biblioteca';
        $data['title2'] = 'Paises';
        $this->load->view('templates/header', $data);
        $this->load->view('paises/list', $data);
        $this->load->view('templates/footer');
    }
    public function add()
    {
        $data['paises'] = $this->paises_model->get();
        $data['main_title'] = 'Biblioteca';
        $data['title2'] = 'Editoriales';
        $this->load->view('templates/header', $data);
        $this->load->view('paises/list', $data);
        $this->load->view('templates/footer');
    }
}
