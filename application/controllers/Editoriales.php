<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editoriales extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('editoriales_model');
        $this->load->helper('url');
        $this->load->library('calendar');
    }
    public function index()
    {
        if(is_logged_in())
        {
            $data['editoriales'] = $this->editoriales_model->get();
            $data['main_title'] = 'Biblioteca';
            $data['title2'] = 'Editoriales';
            $this->load->view('templates/header', $data);
            $this->load->view('editoriales/list', $data);
            $this->load->view('templates/footer');
        }
        else
        {
            redirect('login');
        }
    }
    public function add()
    {
        if(is_logged_in())
        {
            $data['main_title'] = 'Biblioteca';
            $data['title2'] = 'Editoriales';

            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('nombre', 'Nombre', 'required');
            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header', $data);
                $this->load->view('editoriales/add');
                $this->load->view('templates/footer');

            }
            else
            {
                $this->editoriales_model->add();
                $this->load->view('templates/header', $data);
                $this->load->view('editoriales/success');
                $this->load->view('templates/footer');
            }
        }
        else
        {
            redirect('login');
        }

    }
    public function delete()
    {
        if(is_logged_in())
        {
            $id = $this->input->get('id');
            $resultado = $this->editoriales_model->get_editorial_by_id($id);
            if(count($resultado) > 0)
            {
                $this->editoriales_model->delete($id);
                $this->index();
            }
        }
        else
        {
            redirect('login');
        }

    }
    public function edit()
    {
        if(is_logged_in())
        {
            $this->load->helper('form');
            $data['main_title'] = 'Biblioteca';
            $data['title2'] = 'Editoriales';
            // Obtenemos el id de la editorial a editar
            $id = $this->input->get('id');
            $resultado = $this->editoriales_model->get_editorial_by_id($id);
            $nombre = $this->input->post('nombre');
            $data['nombre'] = $nombre;
            if($nombre == NULL)
            {
                if(count($resultado) > 0)
                {
                    $data['editorial'] = $resultado[0];
                    $this->load->view('templates/header', $data);
                    $this->load->view('editoriales/edit', $data);
                    $this->load->view('templates/footer');
                }
                else
                {
                    $data['mensaje'] = 'No existe esa editorial';
                    $this->load->view('templates/header', $data);
                    $this->load->view('templates/error', $data);
                    $this->load->view('templates/footer');
                }
            }
            else
            {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('nombre', 'Nombre', 'required');
                if ($this->form_validation->run() === FALSE)
                {
                    $this->load->view('templates/header', $data);
                    $this->load->view('editoriales/edit');
                    $this->load->view('templates/footer');

                }
                else
                {
                    $id = $this->input->post('id');
                    $this->editoriales_model->update($id, $nombre);
                    $this->index();
                }
            }
        }
        else
        {
            redirect('login');
        }
    }
}
